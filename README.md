# Ansible Role: DaVinci Resolve

This role installs [DaVinci Resolve](https://www.blackmagicdesign.com/products/davinciresolve/) binary on any Linux supported host.

## Requirements

None.

## Installing

The role can be installed by running the following command:

```bash
git clone https://gitlab.com/enmanuelmoreira/ansible-role-davinci-resolve enmanuelmoreira.davinci-resolve
```

Add the following line into your `ansible.cfg` file:

```bash
[defaults]
role_path = ../
```

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

The version of Davinci Resolve:

    davinci_version: 17.4.3

There are specific vars for Debian or other Linux distributions in which will be installed the installer of Davinci Resolve (see `vars folder`)

## Dependencies

None.

## Example Playbook

    - hosts: all
      become: yes
      roles:
        - role: enmanuelmoreira.davinci-resolve

## License

MIT / BSD
